var $ = require('jquery');
var cList;

function Currency(id,numCode,сharCode,nominal,name,value){
	this.id = id;
	this.numCode = numCode;
	this.сharCode = сharCode;
	this.nominal = nominal;
	this.name = name;
	this.value = value;
}; 
Currency.prototype.showInfo = function(name,parent) {
  parent.find('.info_'+name).text(this.name +' курс за '+ this.nominal+' - '+this.value+'(РУБ)');
};

function CurrencyList(source){
	if (source!==undefined) {
		for (var i=0, imax = source.length;i < imax; i++){
			this.add(source[i].ID,
				       source[i].NumCode,
				       source[i].CharCode,
				       source[i].Nominal,
				       source[i].Name,
				       source[i].Value);
		};
    this.load();
	}
};

CurrencyList.prototype = Object.create(Array.prototype);
CurrencyList.prototype.constructor = CurrencyList;
CurrencyList.prototype.add = function(id,numCode,сharCode,nominal,name,value){
	this.push(new Currency(id,numCode,сharCode,nominal,name,value));
};
CurrencyList.prototype.load = function(){
	this.forEach(function(item){
	  	$('.currency').append( $('<option value="'+item.id+'">'+item.сharCode+'</option>'));
		  });	
}
;
CurrencyList.prototype.findById = function(id){
	return this.find(function(item){
		return item.id == id;
	})
};


function setCurrency(divParent,direction,currency,sum){
	$(divParent).find('.currency option:selected').each(function(){this.selected=false;});
	$(divParent).find('.currency [value='+currency.id+']').attr("selected", "selected");
	$(divParent).find('input[name="sum_'+direction+'"]').val(sum);
	currency.showInfo(direction,divParent);
};	

function calcTotal(sourceSum,currFrom,currTo){
	return Math.round(sourceSum*currFrom.value/currFrom.nominal*currTo.nominal/currTo.value*100)/100;
};


function changeCurrency() {
  	cList[this.selectedIndex].showInfo(this.name,$(this).parent());
  	$('input[name="sum_from"]').change();		
};

function changeTotal() {
  	if (this.name == "sum_to") {
  		var sourceSum = calcTotal(this.value,
			 													cList.findById($(this).parents('.variant').find('select[name=to] :selected').val()),
			 													cList.findById($('select[name=from] :selected').val())
			 													);
  		$('input[name="sum_from"]').val(sourceSum);		
  		$('input[name="sum_to"]').not(this).each(function() {
  		$(this).val(calcTotal(sourceSum,
			 											cList.findById($('select[name=from] :selected').val()),
			 											cList.findById($(this).parents('.variant').find('select[name=to] :selected').val())
			 	))});
  	} else {
  		var value = this.value;

  		$('select[name=to] :selected').each(function() {
  		$(this).parents('.variant').find('input[name="sum_to"]').val(calcTotal(value,
			 																	cList.findById($('select[name=from] :selected').val()),
			 																	cList.findById($(this).parents('.variant').find('select[name=to] :selected').val())
			 																	));	
  	});
	}
};



$(function () {

	$('.currency').change(changeCurrency);
	$('input').change(changeTotal);
	$('input').keypress(function(event) {
		if (event.which == 13) {
			this.change();
			return false;
		}
		if (event.which != 8 && event.which != 0 && event.which != 46 && (event.which < 48 || event.which > 57)) {
			return false;
		}
	});
	$('.addVariant').click(function(){
		var newVariant = $('.variant:first').clone(true);
		setCurrency(newVariant,'to',cList[0],calcTotal($('input[name="sum_from"]').val(),cList.findById($('select[name=from] :selected').val()),cList[0]))
		newVariant.appendTo(".destination");

	});

	$.get('http://university.netology.ru/api/currency', {}, function(source){ 
		cList = new CurrencyList(source);
    setCurrency($('.source'),'from',cList[0],1);
    setCurrency($('.destination'),'to',cList[1],calcTotal(1,cList[0],cList[1]));
	});
});